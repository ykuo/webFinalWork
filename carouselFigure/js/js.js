$(function () {
    var i = 0;/*定义索引,图片和小圆点共用*/
    var timer;/*定时函数*/
    $('.carouselFigureBox .carouselImg img').eq(0).show().siblings().hide();/*定义默认的显示图片,索引为0*//*使用jQuery的eq选择器*/

    function start() {/*轮播开始*/
        timer = setInterval(function () {/*自动轮播定时函数*/
                    i++;/*索引累加*/
                    if (i == 4) i = 0;
                    change();/*继续轮播*/
                }, 2000)/*2s一次轮播*/
    };

    function change() {/*图片显示函数,fadeIn和fadeOut图片显示方式为淡入淡出1s*/
        $('.carouselFigureBox .carouselImg img').eq(i).fadeIn(1000).siblings().stop().fadeOut(1000);/*eq选择当前图片,siblings排除其他图片,stop其他图片停止切换,只切换当前图片*/
        $('b').eq(i).addClass('carouselRudis').siblings().removeClass('carouselRudis');/*小圆点颜色改变*/
    }

    start();
    $('b').hover(function() {/*当鼠标运动到小圆点时,切换图片*/
        clearInterval(timer);/*清除定时*/
        i = $(this).index();/*获取当前鼠标运动到的小圆点索引*/
        change();
    });
    $('b').mouseleave(function() {/*当鼠标离开小圆点时,重新开始轮播计时*/
        start();
    });
})
