/*******************************搜索框*******************************/
/*与导航栏模块共用
var $searchText;
var $searchNet;
*/

function blueIcon(x) {
	x.src="searchModule/img/searchIcon_blue.png";
	x.alt="searchIcon_blue";
}

function normalIcon(x) {
	x.src="searchModule/img/searchIcon.png";
	x.alt="searchIcon";
}

function getSearchText() {
	$searchText = document.getElementById("input_searchBox").value;
	document.getElementById("input_searchBox").value = "";
}

function searchNet() {
	getSearchText();
	if ($searchText == "") exit;
	$searchNet = "https://www.baidu.com/s?ie=UTF-8&wd=" + $searchText;
	window.open($searchNet);
}

function getEnterKey(event) {
	var keyCode = event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode);/*获取用户单击键盘的值*//*回车键13*/
	if (keyCode == 13) searchNet();
}
