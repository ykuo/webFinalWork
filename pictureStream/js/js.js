var $picStreamHeight = new Array(5);/*图片流分5列,每列高度数据保存*/
var $selectedI;/*选中第i个列*/
var $i = 0;/*用于懒加载图片计数*/

for (i = 0; i < 5; i++) {
	$picStreamHeight[i] = 0;/*设置图片流初始height值*/
}

function createPicImg() {/*动态创建一个添加图片img*//*创建在最后一个结点*/
	var picImg = document.createElement("img");

	/*设置div的属性*/
	picImg.setAttribute("id", "picImg" + $i);
	picImg.setAttribute("class", "lazyLoadImg");
	picImg.setAttribute("src", "pictureStream/pic/img" + $i + ".jpg");
	picImg.setAttribute("onclick", "openPicUrl(" + $i + ");");

	var whiteImg = document.createElement("img");/*垂直图片间隙填充*/

	whiteImg.setAttribute("class", "whiteImg");
	whiteImg.setAttribute("src", "pictureStream/img/whiteImg.jpg");

	getselectedI();

	document.getElementById("stream" + $selectedI).appendChild(picImg);/*动态插入img*//*插入在最后一个*/
	document.getElementById("stream" + $selectedI).appendChild(whiteImg);
}

/*打开图片*/
function openPicUrl(i) {
	var picSrc = document.getElementById("picImg" + i).src;
	if (picSrc)
	{
		window.open(picSrc);
	}
}

/*页面加载*/
function loadPic() {
	var i = 0;
	while (i < 5 && $i < 43)/*加载一行图片(共43张)*/
	{
		createPicImg();

		$picStreamHeight[$selectedI] += 1;

		$i++;
		i++;
	}
}

/*寻找$picStreamTop[i]最小的列标,记作$selectedI*/
function getselectedI() {
	var minTop = $picStreamHeight[0];/*从左向右查找*/
	$selectedI = 0;

	for (i = 0; i < 5; i++) {
		if ($picStreamHeight[i] < minTop) {
			minTop = $picStreamHeight[i];
			$selectedI = i;
		}
	}
}
