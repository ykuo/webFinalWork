/*******************************每日金句*******************************/
/*创建任意区间的随机数*/
function myRandom(min , max) {
	return Math.round(Math.random() * (max-min) + min);
}

/*每日金句*/
function showWords() {
	switch ( i = myRandom(0 , 9)) {
		case 0: everydayWords.innerHTML = "你相信光吗？";
			break;
		case 1: everydayWords.innerHTML = "闻闻天空的味道";
			break;
		case 2: everydayWords.innerHTML = "一星陨落，黯淡不了星空灿烂";
			break;
		case 3: everydayWords.innerHTML = "鹰击长空， 鱼翔浅底，万类霜天竞自由";
			break;
		case 4: everydayWords.innerHTML = "列嶂青且茜，愿言试长剑";
			break;
		case 5: everydayWords.innerHTML = "鲲鹏展翅九万，翻动扶摇羊角";
			break;
		case 6: everydayWords.innerHTML = "世界仍旧在眼前，但世界永远地改变了";
			break;
		case 7: everydayWords.innerHTML = "没有伤痕累累，哪来皮糙肉厚";
			break;
		case 8: everydayWords.innerHTML = "除了胜利，我们已经无路可走";
			break;
		case 9: everydayWords.innerHTML = "5月15日7时18分，天问一号成功着陆火星";
			break;
	}
}
