/*******************************收藏夹*******************************/
var $likeSum = 0;
var $likeName = new Array(8);
var $likeNet = new Array(8);
var $likeExist = new Array(8);

for (i = 0; i < 8; i++) {
	$likeExist[i] = 0;
}

function recycleShow(x) {
	x.style="border: 1px dashed #797979; display: inherit;";
}

function recycleHide(x) {
	x.style="display: none;";
}

function drag(event, x) {
	recycleShow(x);
	/*设置传递的内容为被拖曳元素的id名称,数据类型为纯文本类型*/
	event.dataTransfer.setData("text/plain", event.target.id);
}

function allowDrop(event) {
	event.preventDefault();/*解禁当前元素为可放置被拖曳元素的区域*/
}

function drop(event, x) {
	allowDrop(event);
	var id = event.dataTransfer.getData("text");/*获取当前被放置的书签id名称*/
	var like = document.getElementById(id);/*根据id名称获取书签*/
	document.getElementById("container").removeChild(like);/*获取收藏夹区域并删除该书签*/
	recycleHide(x);
	var i = id.charAt(id.length - 1);
	$likeExist[i] = 0;
	$likeSum--;
	if (!document.getElementById("likeMore")) creatLikeMoreDiv();/*创建唯一的添加按钮*/
}

function likeAddShow() {
	likeAddBackground.style="display: inherit;";
	likeAdd.style="display: inherit;";
}

function likeAddHide() {
	likeAddBackground.style="display: none;";
	likeAdd.style="display: none;";
}

function openLikeNet(i) {
	window.open($likeNet[i]);
}

function creatLikeDiv(i) {/*动态创建一个书签div*//*创建在第一个*/
	var likeDiv = document.createElement('div');

	/*设置div的属性*/
	likeDiv.setAttribute("id", "like" + i);
	likeDiv.setAttribute("class", "like");
	likeDiv.setAttribute("draggable", "true");
	likeDiv.setAttribute("ondragstart", "drag(event, recycle)");
	likeDiv.style="background: url('bookmarkBar/img/like" + i + ".png') no-repeat";
	likeDiv.setAttribute("onclick", "openLikeNet(" + i + ");");

	likeDiv.innerHTML = $likeName[i].charAt(0);/*获取likeName[i]第一个字符*/
 
 	var firstChild = document.getElementById("container").firstChild;
	document.getElementById("container").insertBefore(likeDiv, firstChild);/*动态插入div*//*插入在第一个*/
}

function creatLikeMoreDiv() {/*动态创建一个添加书签div*//*创建在最后一个结点*/
	var likeDiv = document.createElement('div');

	/*设置div的属性*/
	likeDiv.setAttribute("id", "likeMore");
	likeDiv.setAttribute("class", "like");
	likeDiv.style="background: url('bookmarkBar/img/likeMore.png') no-repeat";
	likeDiv.setAttribute("onclick", "likeAddShow();");
 
	document.getElementById("container").appendChild(likeDiv);/*动态插入div*//*插入在最后一个*/
}

function addLike() {
	likeAddHide();
	for (i = 0; i < 8; i++) {
		if ($likeExist[i] == 0) {
			$likeSum++;
			if ($likeSum == 8) {
				var like = document.getElementById("likeMore");/*根据id名称获取div*/
				document.getElementById("container").removeChild(like);
			}
			$likeName[i] = document.getElementById("input_likeName").value;
			$likeNet[i] = document.getElementById("input_likeNet").value;
			$likeExist[i] = 1;
			creatLikeDiv(i);
			exit;
		}
	}
}
