/******************************音乐模块******************************/
var $playStatus = 1/*默认播放1*/

function showPlayBtn() {
	musicPlayBtn.style="display: inherit";
	if ($playStatus == 1)
	{
		musicPlayBtn.src="musicModule/img/暂停.png";
	} else {
		musicPlayBtn.src="musicModule/img/播放.png";
	}
}

function hidePlayBtn() {
	musicPlayBtn.style="display: none";
}

function clickPlayBtn() {
	if ($playStatus == 1)
	{
		musicPlayBtn.src="musicModule/img/暂停click.png";
	} else {
		musicPlayBtn.src="musicModule/img/播放click.png";
	}
}

function switchPlayStatus() {
	if ($playStatus == 1)
	{
		$playStatus = 0;
		musicPlayBtn.src="musicModule/img/播放.png";
	} else {
		$playStatus = 1;
		musicPlayBtn.src="musicModule/img/暂停.png";
	}
}

/*用于musicPlayBtn*/
function musicPlay() {
	var audio = document.getElementById("musicAudio");
	if (audio != null) {
		if (audio.paused) {/*暂停返回true*/
		audio.play();/*播放*/
		$musicStatus = 1;
		} else {
			audio.pause();/*暂停*/
			$musicStatus = 0;
		}
	}
}
